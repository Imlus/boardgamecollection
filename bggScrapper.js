const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');
const users = require('./users.js');

(async () => {
    // You can read mode here - https://boardgamegeek.com/wiki/page/BGG_XML_API2
    const url = 'https://www.boardgamegeek.com';
    const collectionUrl = '/xmlapi2/collection?';
    const mainURL = [url, collectionUrl].join('');

    let list= ['Name, Max_players, Min_players, Playing_time'];
    let counter = 0;
    for (const user of users) {
        const parameters = [
            `username=${user}`,
            'own=1',
            'excludesubtype=boardgameexpansion',
            'stats=1'
        ];
        const query = [mainURL, parameters.join('&')].join('');
        try {
            const page = await axios.get(query);
            const $ = cheerio.load(page.data, {
                xmlMode: true
            });
            $('item').each(function () {
                const name = $(this).find('name').text();
                const stats = $(this).find('stats');
                let element = [
                    name,
                    stats.attr('maxplayers'),
                    stats.attr('minplaytime'),
                    stats.attr('maxplaytime'),
                    stats.attr('playingtime')
                ].join(',');
                list.push(element);
            });
            counter++;
        } catch (e) {
            console.log('Caught' + e);
            console.log(`Take a look at this error page ${query}`);
        }
    }
    if(!(counter !==  users.length)) {
        //file will be stored in the project folder
        fs.writeFile(`${[new Date().getFullYear(), new Date().getMonth()].join('_')}_Output.txt`, list.join('\n'), (err) => {
            if (err) throw err;
        });

    } else
        console.log(`Not all users have been successfully collected`);
})();
